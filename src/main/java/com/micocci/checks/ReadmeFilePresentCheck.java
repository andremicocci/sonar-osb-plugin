package com.micocci.checks;

import org.sonar.api.server.rule.RulesDefinition;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.squidbridge.annotations.ActivatedByDefault;
import org.sonar.squidbridge.annotations.SqaleConstantRemediation;
import org.sonar.squidbridge.annotations.SqaleSubCharacteristic;

@Rule(
  key = ReadmeFilePresentCheck.RULE_KEY,
  name = "Each Puppet module should contain a \"README.md\" file",
  priority = Priority.MAJOR)
@SqaleSubCharacteristic(RulesDefinition.SubCharacteristics.UNDERSTANDABILITY)
@SqaleConstantRemediation("20min")
@ActivatedByDefault
public class ReadmeFilePresentCheck extends OSBCheckVisitor{
  public static final String RULE_KEY = "ReadmeFilePresent";

}

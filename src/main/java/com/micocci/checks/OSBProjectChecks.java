package com.micocci.checks;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.batch.fs.InputComponent;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.issue.NewIssue;
import org.sonar.api.batch.sensor.issue.NewIssueLocation;

import com.micocci.rules.MikosRulesDefinition;

public class OSBProjectChecks {
	private final SensorContext context;

	private static final Logger LOGGER = LoggerFactory.getLogger(OSBProjectChecks.class);
	
	public OSBProjectChecks(SensorContext context) {
		this.context = context;
	}

	public void reportProjectIssues() {
		if (context.fileSystem().baseDir() != null) {
			// checkTestsDirectoryPresent(context.fileSystem().baseDir(), 0);
			// checkMetadataJsonFilePresent(context.fileSystem().baseDir(), 0);
			checkReadmeFilePresent(context.fileSystem().baseDir(), 0);
		}
	}

	private void checkReadmeFilePresent(File parentFile, int depth) {
		String[] configs = { "osb-cfgplan-tst.xml", "osb-cfgplan-hml.xml", "osb-cfgplan-prd.xml" };
		
		ArrayList<Object> listaConfigs = new ArrayList<>();
		
		for (File file : parentFile.listFiles()) {
			System.out.println("************* FILE NAME : " + file.getName());
			listaConfigs.add(file.getName());
		}
		if(listaConfigs.containsAll(Arrays.asList(configs))) {
			System.out.println("################# Contem configs!");
		}else {
			System.out.println("################# Nao Contem configs!");
			addIssue("Add OSB Config files o the \"" + context.module().key() + "\" OSB module.", context.module());
		}
	}
	
	  //protected void addIssue(String ruleKey, String message, InputComponent inputComponent) {
	  protected void addIssue(String message, InputComponent inputComponent) {
		    LOGGER.info("Adding issue: " + " " + message);
		    NewIssue newIssue = context
		      .newIssue()
		      .forRule(MikosRulesDefinition.RULE_KEY);

		    newIssue.at(newLocation(inputComponent, newIssue, message)).save();
		  }

		  private static NewIssueLocation newLocation(InputComponent input, NewIssue issue, String message) {
		    return issue.newLocation()
		      .on(input).message(message);
		  }
	

}

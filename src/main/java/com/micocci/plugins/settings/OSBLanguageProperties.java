package com.micocci.plugins.settings;

import static java.util.Arrays.asList;

import java.util.List;

import org.sonar.api.config.PropertyDefinition;
import org.sonar.api.resources.Qualifiers;

public class OSBLanguageProperties {
	  public static final String FILE_SUFFIXES_KEY = "sonar.osb.file.suffixes";
	  public static final String FILE_SUFFIXES_DEFAULT_VALUE = ".osb";

	  private OSBLanguageProperties() {
	    // only statics
	  }

	  public static List<PropertyDefinition> getProperties() {
	    return asList(PropertyDefinition.builder(FILE_SUFFIXES_KEY)
	      .defaultValue(FILE_SUFFIXES_DEFAULT_VALUE)
	      .category("OSB")
	      .name("File Suffixes")
	      .description("Comma-separated list of suffixes for files to analyze.")
	      .onQualifiers(Qualifiers.PROJECT)
	      .build());
	  }

}

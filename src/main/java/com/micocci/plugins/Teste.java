package com.micocci.plugins;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class Teste {

	public static void main(String[] args) {
		checkReadmeFilePresent(new File("C:\\JDeveloper\\mywork\\ServiceBusApplication1\\EmissaoOA.DocumentoEmissaoServiceEBS"));

	}
	
	
	private static void checkReadmeFilePresent(File parentFile) {
		String[] configs = { "osb-cfgplan-tst.xml", "osb-cfgplan-hml.xml", "osb-cfgplan-prd.xml" };
		
		ArrayList<Object> listaConfigs = new ArrayList<>();
		
		for (File file : parentFile.listFiles()) {
			System.out.println("************* FILE NAME : " + file.getName());
			listaConfigs.add(file.getName());
		}
		if(listaConfigs.containsAll(Arrays.asList(configs))) {
			System.out.println("################# Contem configs!");
		}else {
			System.out.println("################# Nao Contem configs!");
		}
	}

}

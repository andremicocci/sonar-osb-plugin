package com.micocci.plugins;

import org.sonar.api.Plugin;

import com.micocci.languages.OSBLanguage;
import com.micocci.languages.OSBQualityProfile;
import com.micocci.plugins.hooks.DisplayIssuesInScanner;
import com.micocci.plugins.hooks.DisplayQualityGateStatus;
import com.micocci.plugins.settings.OSBLanguageProperties;
import com.micocci.rules.MikosRulesDefinition;
import com.micocci.rules.OSBSensor;

public class OSBPlugin implements Plugin {

	@Override
	public void define(Context context) {
	    
		context.addExtensions(OSBLanguage.class, OSBQualityProfile.class);
	    context.addExtension(OSBLanguageProperties.getProperties());
		
		context.addExtensions(DisplayIssuesInScanner.class, DisplayQualityGateStatus.class);
	    context.addExtensions(MikosRulesDefinition.class, OSBSensor.class);
	    //context.addExtensions(OSBLintRulesDefinition.class, OSBLintIssuesLoaderSensor.class);
		
	    
	    // tutorial on web extensions
//	    context.addExtension(MyPluginPageDefinition.class);
	    
//	    context.addExtensions(asList(
//	    	      PropertyDefinition.builder("sonar.foo.file.suffixes")
//	    	        .name("Suffixes OSBLint")
//	    	        .description("Suffixes supported by OSBLint")
//	    	        .category("OSBLint")
//	    	        .defaultValue("")
//	    	        .build()));
		
	}

}

package com.micocci.languages;

import org.sonar.api.server.profile.BuiltInQualityProfilesDefinition;

import com.micocci.rules.MikosRulesDefinition;

public class OSBQualityProfile implements BuiltInQualityProfilesDefinition {

	@Override
	public void define(Context context) {
	    NewBuiltInQualityProfile profile = context.createBuiltInQualityProfile("OSBLint Rules", OSBLanguage.KEY);
	    profile.setDefault(true);

	    //NewBuiltInActiveRule rule1 = profile.activateRule(OSBLintRulesDefinition.REPO_KEY, "ExampleRule1");
	    //rule1.overrideSeverity("CRITICAL");
	    
	    NewBuiltInActiveRule rule2 = profile.activateRule(MikosRulesDefinition.REPOSITORY, MikosRulesDefinition.RULE_NAME);
	    rule2.overrideSeverity("CRITICAL");

	    profile.done();
	}

}

package com.micocci.languages;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sonar.api.config.Configuration;
import org.sonar.api.resources.AbstractLanguage;

import com.micocci.plugins.settings.OSBLanguageProperties;

public class OSBLanguage extends AbstractLanguage {

	  public static final String NAME = "OSB";
	  public static final String KEY = "osb";

	  private final Configuration config;

	  public OSBLanguage(Configuration config) {
	    super(KEY, NAME);
	    this.config = config;
	  }

	  @Override
	  public String[] getFileSuffixes() {
	    String[] suffixes = filterEmptyStrings(config.getStringArray(OSBLanguageProperties.FILE_SUFFIXES_KEY));
	    if (suffixes.length == 0) {
	      suffixes = StringUtils.split(OSBLanguageProperties.FILE_SUFFIXES_DEFAULT_VALUE, ",");
	    }
	    return suffixes;
	  }

	  private String[] filterEmptyStrings(String[] stringArray) {
	    List<String> nonEmptyStrings = new ArrayList<>();
	    for (String string : stringArray) {
	      if (StringUtils.isNotBlank(string.trim())) {
	        nonEmptyStrings.add(string.trim());
	      }
	    }
	    return nonEmptyStrings.toArray(new String[nonEmptyStrings.size()]);
	  }

}

package com.micocci.rules;

import org.sonar.api.batch.sensor.Sensor;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.SensorDescriptor;

import com.micocci.checks.OSBProjectChecks;

public class OSBSensor implements Sensor {

	@Override
	public void describe(SensorDescriptor descriptor) {
	    descriptor.name("##MIKOS## OSBLint Issues Loader Sensor");
	    //descriptor.onlyOnLanguage("xml");
	}

	@Override
	public void execute(SensorContext context) {
		new OSBProjectChecks(context).reportProjectIssues();
	}

}

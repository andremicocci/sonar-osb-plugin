package com.micocci.rules;

import org.sonar.api.rule.RuleKey;
import org.sonar.api.rule.RuleStatus;
import org.sonar.api.rule.Severity;
import org.sonar.api.server.rule.RulesDefinition;

public class MikosRulesDefinition implements RulesDefinition {

	public static final String REPOSITORY = "osb-mikos";
	public static final String XML_LANGUAGE = "osb";
	public static final String RULE_NAME = "absent-config-plan";
	public static final RuleKey RULE_KEY = RuleKey.of(REPOSITORY, RULE_NAME);

	@Override
	public void define(Context context) {
		NewRepository repository = context.createRepository(REPOSITORY, XML_LANGUAGE).setName("Mikos OSB Analyzer");

		NewRule x1Rule = repository.createRule(RULE_KEY.rule())
				.setName("Absent OSB Config Plans")
				//.setHtmlDescription("Absent OSB Config Plans \r\n " + 
				//		"All the files below must be included in the project root. \r\n"+
				//		"osb-cfgplan-tst.xml \r\n osb-cfgplan-hml.xml \r\n osb-cfgplan-prd.xml")
				
				.setHtmlDescription(getClass().getResource("/html/" + RULE_NAME + ".html"))

				// optional tags
				.setTags("configuration", "config-plan")

				// optional status. Default value is READY.
				.setStatus(RuleStatus.READY)

				// default severity when the rule is activated on a Quality profile. Default
				// value is MAJOR.
				.setSeverity(Severity.BLOCKER)
				.setActivatedByDefault(true);

		x1Rule.setDebtRemediationFunction(x1Rule.debtRemediationFunctions().linearWithOffset("1h", "30min"));

		// don't forget to call done() to finalize the definition
		repository.done();
	}

}
